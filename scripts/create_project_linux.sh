#!/bin/bash

source shared.sh

echo PROJECT_DIR: $PROJECT_DIR

CMD_BUILD_NAME=linux-x86-64-debug
CMD_BUILD_DIR=$PROJECT_DIR/build/$CMD_BUILD_NAME
CMD_INCLUDE_DIRECTORY="-I$PROJECT_DIR/src $(pkg-config --cflags-only-I gtk+-2.0 gtkglext-1.0)"
export CMD_INSTALL_DIRECTORY=$CMD_BUILD_DIR/install

echo CMD_BUILD_DIR: $CMD_BUILD_DIR

mkdir -p $CMD_INSTALL_DIRECTORY

export CMAKE_CXX_FLAGS="$CMD_SHORT_DEBUG_MODE \
        $CMD_INCLUDE_DIRECTORY/src"
#        -fexceptions"

export CMD_EXE_LINKER_FLAGS="$(pkg-config --libs gtk+-2.0 gtkglext-1.0) -lX11 -lcef -lEGL -lGLESv2 -lwidevinecdmadapter"

OPTIND=1
SUBSTITUTE_FILE_PROC --src=$PROJECT_DIR/scripts/cmake.env.in --dst=$PROJECT_DIR/cmake.env

cd ../

cd ${CMD_BUILD_DIR}

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../..

#make clean
make
#make install