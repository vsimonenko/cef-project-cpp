#!/bin/bash

function SUBSTITUTE_FILE_PROC() {
	for i in "$@"
    do
        case ${i} in
        --src=*)
            SRC_PATH=${i#*=}
        ;;
        --dst=*)
            DST_PATH=${i#*=}
        ;;
        esac
    done
    cp $SRC_PATH $DST_PATH

    unset IFS
    for var in $(compgen -e); do
            txt=${!var}
            ln=$(echo ${txt//\`/\'})
            sed -i "s\`\${$var}\`$ln\`g" $DST_PATH
    done
}

export -f SUBSTITUTE_FILE_PROC

export PROJECT_DIR=$(pwd | sed 's/\(.*\)\/scripts$/\1/');