include_directories(${CEF_ROOT}/include)


set(CEFSIMPLE_SRCS_TEST_LINUX
        client_util_unittest.cc
        run_all_unittests.cc
        )


# Target executable names.
set(CEF_TARGET "cefsimpletest")

# Logical target used to link the libcef library.
ADD_LOGICAL_TARGET("libcef_lib" "${CEF_LIB_DEBUG}" "${CEF_LIB_RELEASE}")

# Determine the target output directory.
SET_CEF_TARGET_OUT_DIR()

#
# Linux configuration.
#

set(CMAKE_C_FLAGS "-fexceptions")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fexceptions")

set(EXE_LINKER_FLAGS "${EXE_LINKER_FLAGS}  -lcef_gtest -lcef_dll_wrapper -L${CEF_TARGET_OUT_DIR} -L${CMAKE_CURRENT_BINARY_DIR}/../libcef_dll_wrapper -L${CMAKE_CURRENT_BINARY_DIR}/../third_party/cef/cef_binary_3.3282.1735.g1e5b631_linux64/tests/gtest")

message("EXE_LINKER_FLAGS: " ${EXE_LINKER_FLAGS})

if (OS_LINUX)
    FIND_LINUX_LIBRARIES("glib-2.0")

    message("***** OS_LINUX *****" "")

    add_executable(${CEF_TARGET} ${CEFSIMPLE_SRCS_TEST_LINUX})

    SET_EXECUTABLE_TARGET_PROPERTIES(${CEF_TARGET})
    add_dependencies(${CEF_TARGET} libcef_dll_wrapper cef_gtest cefsimple)
    target_link_libraries(${CEF_TARGET} libcef_lib libcef_dll_wrapper cef_gtest ${CEF_STANDARD_LIBS})

    # Set rpath so that libraries can be placed next to the executable.
    set_target_properties(${CEF_TARGET} PROPERTIES INSTALL_RPATH "$ORIGIN")
    set_target_properties(${CEF_TARGET} PROPERTIES LINK_LIBRARIES ${EXE_LINKER_FLAGS})
    set_target_properties(${CEF_TARGET} PROPERTIES BUILD_WITH_INSTALL_RPATH TRUE)
    set_target_properties(${CEF_TARGET} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CEF_TARGET_OUT_DIR})

    set(CMD_COPY1 COMMAND cp -r "${CEF_BINARY_DIR}/*" "${CEF_TARGET_OUT_DIR}/")
    set(CMD_COPY2 COMMAND cp -r "${CEF_ROOT}/Resources/*" "${CEF_TARGET_OUT_DIR}/")
    set(CMD_COPY3 COMMAND cp -r "${CMAKE_SOURCE_DIR}/resources/*" "${CEF_TARGET_OUT_DIR}/resources")
    set(CMD_RES1 COMMAND mkdir -p "${CEF_TARGET_OUT_DIR}/resources")
    add_custom_command(
            TARGET ${CEF_TARGET}
            PRE_BUILD
            ${CMD_RES1}
            ${CMD_COPY1}
            ${CMD_COPY2}
            ${CMD_COPY3}
    )
    # Copy binary and resource files to the target output directory.
    COPY_FILES("${CEF_TARGET}" "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CEF_TARGET_OUT_DIR}")
endif ()
