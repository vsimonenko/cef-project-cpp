#include <tests/gtest/include/gtest/gtest.h>

//export GTEST_FILTER = "ParseUrlTest*"
//--gtest_filter=ParseUrlTest*
GTEST_API_ int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    ::testing::GTEST_FLAG(filter) = "ParseUrlTest*";
    return RUN_ALL_TESTS();
}
