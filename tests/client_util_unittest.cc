#include <stdexcept>
#include "tests/gtest/include/gtest/gtest.h"
#include <iostream>
#include <string>
#include <regex>
#include <internal/cef_types_wrappers.h>
#include "../src/client_util.h"

TEST(ParseUrlTest, Params) {
    std::map<std::string, std::string> *params = new std::map<std::string, std::string>();
    std::string s ("JS_FACTORY_NAME=js::TestJSFactory&TEST1=test1_");
    std::cmatch m;
    std::regex e ("(^|&)([a-zA-Z_0-9]+)=([a-zA-Z_:0-9]+)(&|$)");

    while (std::regex_search(s.c_str(), m, e)) {
      std::cout << "3: " << m[2] << '\n';
      std::cout << "4: " << m[3] << '\n';
      params->insert(std::make_pair(m[2], m[3]));
      s = m.suffix().str();
    }

    for (auto it = params->begin(); it != params->end(); ++it) {
        std::cout << (*it).first << "=" << (*it).second << '\n';
    }
}

//GTEST_API_ int main(int argc, char **argv) {
//    testing::InitGoogleTest(&argc, argv);
//    return RUN_ALL_TESTS();
//}
