#include <wrapper/cef_helpers.h>
#include "client.h"

bool client::ClientHandler::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
                                                     CefRefPtr<CefProcessMessage> message) {
    CEF_REQUIRE_UI_THREAD();
    std::string name = message->GetName().ToString();
    if (name == "calculate") {
        std::string v = message->GetArgumentList()->GetString(0).ToString();
        double d1 = atof(v.c_str());
        v = message->GetArgumentList()->GetString(1).ToString();
        double d2 = atof(v.c_str());
        std::string op = message->GetArgumentList()->GetString(2).ToString();

        double result = 0;
        if (op.compare("+") == 0)
            result = d1 + d2;
        if (op.compare("-") == 0)
            result = d1 - d2;
        if (op.compare("*") == 0)
            result = d1 * d2;
        if (op.compare("/") == 0)
            result = d1 / d2;
        CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("calculate_result");
        CefRefPtr<CefListValue> args = msg->GetArgumentList();
        args->SetString(0, std::to_string(result));
        browser->SendProcessMessage(PID_RENDERER, msg);

        msg = CefProcessMessage::Create("log_history");
        args = msg->GetArgumentList();
        args->SetString(0, "calculate: " + std::to_string((d1)) + " " + op + " " + std::to_string((d2)) + " = " + std::to_string((result)));
        browser->SendProcessMessage(PID_RENDERER, msg);

        return true;
    }
    if (name.compare("to_page") == 0) {
        int page = message->GetArgumentList()->GetInt(0);
        if (page == 1)
            browser->GetMainFrame()->LoadURL("client://page1.html");
        else
            browser->GetMainFrame()->LoadURL("client://page2.html");
    }
    return false;
}
