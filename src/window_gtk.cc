#include <base/cef_logging.h>
#include <gdk/gdkx.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkwindow.h>
#include "window_gtk.h"

namespace client {
    CefWindowHandle WindowGtk::GetXWindow() {
        ::Window top_xwindow = GDK_WINDOW_XID(gtk_widget_get_window(top_window));
        DCHECK(top_xwindow);
        return top_xwindow;
    }

    GtkWidget *WindowGtk::GetTopWindow() {
        return top_window;
    }

    WindowGtk::WindowGtk(CefRect *windowRect, GtkWidget *top_window) : Window(windowRect),
                                                                       top_window(top_window) {}

    WindowGtk::WindowGtk(GtkWidget *top_window) : top_window(top_window) {}

    void WindowGtk::PlatformTitleChange(CefRefPtr<CefBrowser> browser,
                                        const CefString &title) {
        std::string titleStr(title);

        // Retrieve the X11 display shared with Chromium.
        ::Display *display = cef_get_xdisplay();
        DCHECK(display);

        // Retrieve the X11 window handle for the browser.
        ::Window window = browser->GetHost()->GetWindowHandle();
        DCHECK(window != kNullWindowHandle);

        // Retrieve the atoms required by the below XChangeProperty call.
        const char *kAtoms[] = {"_NET_WM_NAME", "UTF8_STRING"};
        Atom atoms[2];
        int result =
                XInternAtoms(display, const_cast<char **>(kAtoms), 2, false, atoms);
        if (!result)
            NOTREACHED();

        // Set the window title.
        XChangeProperty(display, window, atoms[0], atoms[1], 8, PropModeReplace,
                        reinterpret_cast<const unsigned char *>(titleStr.c_str()),
                        titleStr.size());

        // TODO(erg): This is technically wrong. So XStoreName and friends expect
        // this in Host Portable Character Encoding instead of UTF-8, which I believe
        // is Compound Text. This shouldn't matter 90% of the time since this is the
        // fallback to the UTF8 property above.
        XStoreName(display, browser->GetHost()->GetWindowHandle(), titleStr.c_str());
    }

    void WindowGtk::SetXWindowBounds(CefWindowHandle xwindow, int x, int y, size_t width, size_t height) {
        ::Display *xdisplay = cef_get_xdisplay();
        XWindowChanges changes = {0};
        changes.x = x;
        changes.y = y;
        changes.width = static_cast<int>(width);
        changes.height = static_cast<int>(height);
        XConfigureWindow(xdisplay, xwindow, CWX | CWY | CWHeight | CWWidth, &changes);
    }

    void WindowGtk::SetXWindowBounds(CefWindowHandle xwindow, CefRect *rect) {
        SetXWindowBounds(xwindow, rect->x, rect->y, rect->width, rect->height);
    }

    void WindowGtk::SetXWindowVisible(CefWindowHandle xwindow, bool visible) {
        ::Display *xdisplay = cef_get_xdisplay();

        // Retrieve the atoms required by the below XChangeProperty call.
        const char *kAtoms[] = {"_NET_WM_STATE", "ATOM", "_NET_WM_STATE_HIDDEN"};
        Atom atoms[3];
        int result =
                XInternAtoms(xdisplay, const_cast<char **>(kAtoms), 3, false, atoms);
        if (!result)
            NOTREACHED();

        if (!visible) {
            // Set the hidden property state value.
            scoped_ptr<Atom[]> data(new Atom[1]);
            data[0] = atoms[2];

            XChangeProperty(xdisplay, xwindow,
                            atoms[0],  // name
                            atoms[1],  // type
                            32,        // size in bits of items in 'value'
                            PropModeReplace,
                            reinterpret_cast<const unsigned char *>(data.get()),
                            1);  // num items
        } else {
            // Set an empty array of property state values.
            XChangeProperty(xdisplay, xwindow,
                            atoms[0],  // name
                            atoms[1],  // type
                            32,        // size in bits of items in 'value'
                            PropModeReplace, NULL,
                            0);  // num items
        }
    }

    void WindowGtk::ChangeConfigure(GtkWidget *widget, GdkEvent *event, WindowGtk *window) {
        if (event->type != GDK_CONFIGURE)
            return;
        GdkRectangle sz;
        sz.x = window->GetWindowRect().x;
        sz.y = window->GetWindowRect().y;
        sz.height = window->GetWindowRect().height + 1;
        sz.width = window->GetWindowRect().width;
        gtk_widget_size_allocate(widget, &sz);
    }

    void WindowGtk::CefBrowserSizeAllocated(GtkWidget *widget, GtkAllocation *allocation, WindowGtk *window) {
        // Offset browser positioning by any controls that will appear in the client area.
        // Size the browser window to match the GTK widget.
        ::Window xwindow = window->GetBrowser().get()->GetHost()->GetWindowHandle();
        DCHECK(xwindow);

        window->ChangeRect(allocation);
    }

    void WindowGtk::SetBrowserRect(CefRect *rect) {
        Window::SetBrowserRect(rect);
        dx = rect->x - GetWindowRect().x;
        dy = rect->y - GetWindowRect().y;
        dw = GetWindowRect().width - rect->width;
        dh = GetWindowRect().height - rect->height;
        ::Window xwindow = GetBrowser().get()->GetHost()->GetWindowHandle();
        DCHECK(xwindow);
        client::WindowGtk::SetXWindowBounds(xwindow, rect);
    }

    void WindowGtk::Show() {
        ::Window xwindow = GetBrowser().get()->GetHost()->GetWindowHandle();
        DCHECK(xwindow);
        CefRect rect = GetBrowserRect();
        client::WindowGtk::SetXWindowBounds(xwindow, &rect);
        client::WindowGtk::SetXWindowVisible(xwindow, true);
    }

    void WindowGtk::ChangeRect(GdkRectangle *newSize) {
        const int x = newSize->x + dx;
        const int y = newSize->y + dy;
        const int width = newSize->width - dw;
        const int height = newSize->height - dh;
        SetWindowRect(new CefRect(newSize->x, newSize->y, newSize->width, newSize->height));
        SetBrowserRect(new CefRect(x, y, width, height));
    }

    gboolean WindowGtk::WindowFocusIn(GtkWidget *widget, GdkEventFocus *event, client::Window *window) {
        if (event->in && window && window->GetBrowser() && window->GetBrowser()->GetHost()) {
            window->GetBrowser()->GetHost()->SetFocus(true);
            // Return true for a windowed browser so that focus is not passed to GTK.
            return TRUE;
        }

        return FALSE;
    }
}
