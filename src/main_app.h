#ifndef CEF_TESTS_CEFSIMPLE_MAIN_APP_H_
#define CEF_TESTS_CEFSIMPLE_MAIN_APP_H_

#include <gtk/gtk.h>
#include "include/cef_app.h"
#include "simple_handler.h"
#include "window.h"
#include "jshandler.h"
#include "client.h"

class MainApp : public CefApp, public CefBrowserProcessHandler, public CefRenderProcessHandler {
public:
    MainApp(int argc, char **argv, std::string js_factory_name);

    CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() OVERRIDE { return this; }

    CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() OVERRIDE { return this; }

    void OnContextInitialized() OVERRIDE;

    bool CreateBrowser(client::Window *window,
                       const std::string &def_url,
                       SimpleHandler *simpleHandler);

    void OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
                          CefRefPtr<CefV8Context> context) OVERRIDE;

    void OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
                           CefRefPtr<CefV8Context> context) override;

    void OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
                              CefRefPtr<CefDOMNode> node) OVERRIDE;

    bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
                                  CefRefPtr<CefProcessMessage> message) override;

    bool Close();

private:
    CefRefPtr<SimpleHandler> simpleHandler;
    std::string js_factory_name;
    js::V8ContextCallbackMap callbackMap;

IMPLEMENT_REFCOUNTING(MainApp);
};

#endif  // CEF_TESTS_CEFSIMPLE_MAIN_APP_H_
