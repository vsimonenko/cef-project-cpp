#include <X11/Xlib.h>
#include <cef_parser.h>
#include <regex>
#include "client_util.h"

namespace shared {

    std::string DumpRequestContents(CefRefPtr<CefRequest> request) {
        std::stringstream ss;

        ss << "URL: " << std::string(request->GetURL());
        ss << "\nMethod: " << std::string(request->GetMethod());

        CefRequest::HeaderMap headerMap;
        request->GetHeaderMap(headerMap);
        if (!headerMap.empty()) {
            ss << "\nHeaders:";
            CefRequest::HeaderMap::const_iterator it = headerMap.begin();
            for (; it != headerMap.end(); ++it) {
                ss << "\n\t" << std::string((*it).first) << ": "
                   << std::string((*it).second);
            }
        }

        CefRefPtr<CefPostData> postData = request->GetPostData();
        if (postData.get()) {
            CefPostData::ElementVector elements;
            postData->GetElements(elements);
            if (!elements.empty()) {
                ss << "\nPost Data:";
                CefRefPtr<CefPostDataElement> element;
                CefPostData::ElementVector::const_iterator it = elements.begin();
                for (; it != elements.end(); ++it) {
                    element = (*it);
                    if (element->GetType() == PDE_TYPE_BYTES) {
                        // the element is composed of bytes
                        ss << "\n\tBytes: ";
                        if (element->GetBytesCount() == 0) {
                            ss << "(empty)";
                        } else {
                            // retrieve the data.
                            size_t size = element->GetBytesCount();
                            char *bytes = new char[size];
                            element->GetBytes(size, bytes);
                            ss << std::string(bytes, size);
                            delete[] bytes;
                        }
                    } else if (element->GetType() == PDE_TYPE_FILE) {
                        ss << "\n\tFile: " << std::string(element->GetFile());
                    }
                }
            }
        }

        return ss.str();
    }

    CefRefPtr<CefV8Value> CefValueToCefV8Value(CefRefPtr<CefValue> value) {
        CefRefPtr<CefV8Value> result;
        switch (value->GetType()) {
            case VTYPE_INVALID:
            {
                //std::cout << "Type: VTYPE_INVALID" << std::endl;
                result = CefV8Value::CreateNull();
            }
                break;
            case VTYPE_NULL:
            {
                //std::cout << "Type: VTYPE_NULL" << std::endl;
                result = CefV8Value::CreateNull();
            }
                break;
            case VTYPE_BOOL:
            {
                //std::cout << "Type: VTYPE_BOOL" << std::endl;
                result = CefV8Value::CreateBool(value->GetBool());
            }
                break;
            case VTYPE_INT:
            {
                //std::cout << "Type: VTYPE_INT" << std::endl;
                result = CefV8Value::CreateInt(value->GetInt());
            }
                break;
            case VTYPE_DOUBLE:
            {
                //std::cout << "Type: VTYPE_DOUBLE" << std::endl;
                result = CefV8Value::CreateDouble(value->GetDouble());
            }
                break;
            case VTYPE_STRING:
            {
                //std::cout << "Type: VTYPE_STRING" << std::endl;
                result = CefV8Value::CreateString(value->GetString());
            }
                break;
            case VTYPE_BINARY:
            {
                //std::cout << "Type: VTYPE_BINARY" << std::endl;
                result = CefV8Value::CreateNull();
            }
                break;
            case VTYPE_DICTIONARY:
            {
                //std::cout << "Type: VTYPE_DICTIONARY" << std::endl;
                result = CefV8Value::CreateObject(NULL, NULL);
                CefRefPtr<CefDictionaryValue> dict = value->GetDictionary();
                CefDictionaryValue::KeyList keys;
                dict->GetKeys(keys);
                for (unsigned int i = 0; i < keys.size(); i++) {
                    CefString key = keys[i];
                    result->SetValue(key, CefValueToCefV8Value(dict->GetValue(key)), V8_PROPERTY_ATTRIBUTE_NONE);
                }
            }
                break;
            case VTYPE_LIST:
            {
                //std::cout << "Type: VTYPE_LIST" << std::endl;
                CefRefPtr<CefListValue> list = value->GetList();
                int size = list->GetSize();
                result = CefV8Value::CreateArray(size);
                for (int i = 0; i < size; i++) {
                    result->SetValue(i, CefValueToCefV8Value(list->GetValue(i)));
                }
            }
                break;
        }
        return result;
    }


    std::string GetPathFromURL(CefString url) {
        CefURLParts parts;
        if (ParseUrl(url, parts))
            return GetPathFromURL(parts);
        else
            return "";
    }

    std::string GetPathFromURL(CefURLParts &parts) {
        std::string path = CefString(&parts.path).ToString();
        if (!path.empty())
            return CefString(&parts.path).ToString().substr(2, std::string::npos);
        else
            return "";
    }

    bool ParseUrl(CefString url, CefURLParts &parts) {
        if (url.empty())
            return false;
        CefString cefUrl;
        cefUrl.FromString(url);
        return CefParseURL(cefUrl, parts);
    }

    bool ParseUrl(std::string url, CefURLParts &parts) {
        if (url.empty())
            return false;
        CefString cefUrl;
        cefUrl.FromString(url);
        return ParseUrl(cefUrl, parts);
    }

    std::map<std::string, std::string> *GetParamsFromURL(CefURLParts &parts) {
        std::string s = CefString(&parts.query).ToString();
        std::map<std::string, std::string> *params = new std::map<std::string, std::string>();
        if (s.empty())
            return params;
        std::cmatch m;
        std::regex e ("(^|&)([a-zA-Z_0-9]+)=([a-zA-Z_:0-9]+)(&|$)");
        while (std::regex_search(s.c_str(), m, e)) {
            params->insert(std::make_pair(m[2], m[3]));
            s = m.suffix().str();
        }
        return params;
    }

    void SetList(CefRefPtr<CefListValue> list, CefRefPtr<CefV8Value> args) {
        for (size_t i = 0; i < list->GetSize(); i++) {
            CefRefPtr<CefValue> v = list->GetValue(i);
            args->SetValue(i, CefV8Value::CreateString(v->GetString()));
        }
    }
}  // namespace shared
