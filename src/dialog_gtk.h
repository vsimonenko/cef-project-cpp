#ifndef CEF_DIALOG_H
#define CEF_DIALOG_H


#include <gtk/gtk.h>
#include "window.h"
#include "window_gtk.h"
#include <gtk/gtkstyle.h>

namespace js {

    class CefJSDialogHandlerDelegateGtk : public CefJSDialogHandler {
    public:
        explicit CefJSDialogHandlerDelegateGtk(client::WindowGtk *window);

        bool OnJSDialog(CefRefPtr<CefBrowser> browser, const CefString &origin_url, JSDialogType dialog_type,
                        const CefString &message_text, const CefString &default_prompt_text,
                        CefRefPtr<CefJSDialogCallback> callback, bool &suppress_message) override;

        bool OnBeforeUnloadDialog(CefRefPtr<CefBrowser> browser, const CefString &message_text, bool is_reload,
                                  CefRefPtr<CefJSDialogCallback> callback) override;

        void OnResetDialogState(CefRefPtr<CefBrowser> browser) override;

    private:
        static void OnDialogResponse(GtkDialog *dialogGtk,
                                     gint response_id,
                                     CefJSDialogHandlerDelegateGtk *dialog);

        void Close();

        client::WindowGtk *window = nullptr;
        CefRefPtr<CefJSDialogCallback> js_dialog_callback;
        GtkWidget *gtk_dialog = nullptr;

    IMPLEMENT_REFCOUNTING(CefJSDialogHandlerDelegateGtk);
    };
}


#endif //CEF_DIALOG_H
