#include <cef_browser.h>
#include <cef_jsdialog_handler.h>
#include <wrapper/cef_helpers.h>
#include <cef_parser.h>
#include <gdk/gdkx.h>
#include "dialog_gtk.h"
#include "client.h"


namespace js {

    const char kPromptTextId[] = "cef_prompt_text";

    bool CefJSDialogHandlerDelegateGtk::OnJSDialog(CefRefPtr<CefBrowser> browser, const CefString &origin_url,
                                                   CefJSDialogHandler::JSDialogType dialog_type,
                                                   const CefString &message_text, const CefString &default_prompt_text,
                                                   CefRefPtr<CefJSDialogCallback> callback, bool &suppress_message) {
        CEF_REQUIRE_UI_THREAD();

        if (gtk_dialog)
            return false;

        GtkButtonsType buttons = GTK_BUTTONS_NONE;
        GtkMessageType gtk_message_type = GTK_MESSAGE_OTHER;
        std::string title;

        switch (dialog_type) {
            case JSDIALOGTYPE_ALERT:
                buttons = GTK_BUTTONS_NONE;
                gtk_message_type = GTK_MESSAGE_WARNING;
                title = "JavaScript Alert";
                break;

            case JSDIALOGTYPE_CONFIRM:
                buttons = GTK_BUTTONS_CANCEL;
                gtk_message_type = GTK_MESSAGE_QUESTION;
                title = "JavaScript Confirm";
                break;

            case JSDIALOGTYPE_PROMPT:
                buttons = GTK_BUTTONS_CANCEL;
                gtk_message_type = GTK_MESSAGE_QUESTION;
                title = "JavaScript Prompt";
                break;
        }

        js_dialog_callback = callback;

        if (!origin_url.empty()) {
            title += " - ";
            title += CefFormatUrlForSecurityDisplay(origin_url).ToString();
        }


        GtkWindow *gtkWindow = GTK_WINDOW(window->GetTopWindow());
        if (!gtkWindow)
            return false;

        gtk_dialog = gtk_message_dialog_new(gtkWindow, GTK_DIALOG_MODAL,
                                            gtk_message_type, buttons, "%s",
                                            message_text.ToString().c_str());
        g_signal_connect(gtk_dialog, "delete-event",
                         G_CALLBACK(gtk_widget_hide_on_delete), NULL);

        gtk_window_set_title(GTK_WINDOW(gtk_dialog), title.c_str());

        GtkWidget *ok_button = gtk_dialog_add_button(GTK_DIALOG(gtk_dialog), GTK_STOCK_OK, GTK_RESPONSE_OK);

        if (dialog_type != JSDIALOGTYPE_PROMPT)
            gtk_widget_grab_focus(ok_button);

        if (dialog_type == JSDIALOGTYPE_PROMPT) {
            GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(gtk_dialog));
            GtkWidget *text_box = gtk_entry_new();
            gtk_entry_set_text(GTK_ENTRY(text_box),
                               default_prompt_text.ToString().c_str());
            gtk_box_pack_start(GTK_BOX(content_area), text_box, TRUE, TRUE, 0);
            g_object_set_data(G_OBJECT(gtk_dialog), kPromptTextId, text_box);
            gtk_entry_set_activates_default(GTK_ENTRY(text_box), TRUE);
        }

        gtk_dialog_set_default_response(GTK_DIALOG(gtk_dialog), GTK_RESPONSE_OK);
        g_signal_connect(gtk_dialog, "response", G_CALLBACK(&OnDialogResponse), this);
        gtk_window_set_position(GTK_WINDOW(gtk_dialog), GTK_WIN_POS_CENTER_ALWAYS);
        gtk_widget_show_all(GTK_WIDGET(gtk_dialog));

        return true;
    }

    bool
    CefJSDialogHandlerDelegateGtk::OnBeforeUnloadDialog(CefRefPtr<CefBrowser> browser, const CefString &message_text,
                                                        bool is_reload, CefRefPtr<CefJSDialogCallback> callback) {
        CEF_REQUIRE_UI_THREAD();

        const std::string &new_message_text =
                message_text.ToString() + "\n\nIs it OK to leave/reload this page?";
        bool suppress_message = false;

        return OnJSDialog(browser, CefString(), JSDIALOGTYPE_CONFIRM,
                          new_message_text, CefString(), callback, suppress_message);
    }

    std::string GetPromptText(GtkDialog *dialog) {
        GtkWidget *widget = static_cast<GtkWidget *>(
                g_object_get_data(G_OBJECT(dialog), "cef_prompt_text"));
        if (widget)
            return gtk_entry_get_text(GTK_ENTRY(widget));
        return std::string();
    }

    void CefJSDialogHandlerDelegateGtk::OnDialogResponse(GtkDialog *dialogGtk,
                                                         gint response_id,
                                                         CefJSDialogHandlerDelegateGtk *dialog) {
        CEF_REQUIRE_UI_THREAD();

        switch (response_id) {
            case GTK_RESPONSE_OK:
                dialog->js_dialog_callback->Continue(false, GetPromptText(dialogGtk));
                break;
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
                dialog->js_dialog_callback->Continue(false, CefString());
                break;
            default:
                NOTREACHED();
        }

        dialog->Close();
    }

    void CefJSDialogHandlerDelegateGtk::OnResetDialogState(CefRefPtr<CefBrowser> browser) {
        CEF_REQUIRE_UI_THREAD();
        Close();
    }

    void CefJSDialogHandlerDelegateGtk::Close() {
        if (!gtk_dialog)
            return;
        gtk_widget_destroy(gtk_dialog);
        gtk_dialog = nullptr;
        js_dialog_callback = nullptr;
    }

    CefJSDialogHandlerDelegateGtk::CefJSDialogHandlerDelegateGtk(client::WindowGtk *window) : window(window) {}
}
