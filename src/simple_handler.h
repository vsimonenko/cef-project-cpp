#ifndef CEF_TESTS_CEFSIMPLE_SIMPLE_HANDLER_H_
#define CEF_TESTS_CEFSIMPLE_SIMPLE_HANDLER_H_

#include "include/cef_client.h"
#include "window.h"
#include "dialog_gtk.h"

#include <list>
#include <include/wrapper/cef_helpers.h>
#include <include/cef_render_process_handler.h>
#include "dialog_gtk.h"
#include "client.h"

class SimpleHandler : public CefClient,
                      public CefDisplayHandler,
                      public CefLifeSpanHandler,
                      public CefLoadHandler,
                      public CefJSDialogHandler {
public:
    explicit SimpleHandler(client::Window *window, CefRefPtr<CefJSDialogHandler> dialogHandler,
                           client::ClientHandler *clientHandler);

    ~SimpleHandler() override;

    CefRefPtr<CefDisplayHandler> GetDisplayHandler() OVERRIDE {
        return this;
    }

    CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() OVERRIDE {
        return this;
    }

    CefRefPtr<CefLoadHandler> GetLoadHandler() OVERRIDE { return this; }

    CefRefPtr<CefJSDialogHandler> GetJSDialogHandler() OVERRIDE { return this; }

    void OnTitleChange(CefRefPtr<CefBrowser> browser,
                       const CefString &title) OVERRIDE;

    void OnAfterCreated(CefRefPtr<CefBrowser> browser) OVERRIDE;

    bool DoClose(CefRefPtr<CefBrowser> browser) OVERRIDE;

    void OnBeforeClose(CefRefPtr<CefBrowser> browser) OVERRIDE;

    bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
                                  CefRefPtr<CefProcessMessage> message) override;

    void OnLoadError(CefRefPtr<CefBrowser> browser,
                     CefRefPtr<CefFrame> frame,
                     ErrorCode errorCode,
                     const CefString &errorText,
                     const CefString &failedUrl) OVERRIDE;

    // Request that all existing browser windows close.
    void CloseAllBrowsers(bool force_close);

    bool IsClosing() const { return is_closing_; }

    bool OnJSDialog(CefRefPtr<CefBrowser> browser,
                    const CefString &origin_url,
                    JSDialogType dialog_type,
                    const CefString &message_text,
                    const CefString &default_prompt_text,
                    CefRefPtr<CefJSDialogCallback> callback,
                    bool &suppress_message) OVERRIDE {
        CEF_REQUIRE_UI_THREAD();
        if (dialog_)
            return dialog_->OnJSDialog(browser, origin_url, dialog_type, message_text, default_prompt_text, callback,
                                       suppress_message);
        else
            return CefJSDialogHandler::OnJSDialog(browser, origin_url, dialog_type, message_text, default_prompt_text,
                                                  callback, suppress_message);
    }

    virtual bool OnBeforeUnloadDialog(CefRefPtr<CefBrowser> browser,
                                      const CefString &message_text,
                                      bool is_reload,
                                      CefRefPtr<CefJSDialogCallback> callback) OVERRIDE {
        if (dialog_)
            return dialog_->OnBeforeUnloadDialog(browser, message_text, is_reload, callback);
        else
            return CefJSDialogHandler::OnBeforeUnloadDialog(browser, message_text, is_reload, callback);
    }

    void OnResetDialogState(CefRefPtr<CefBrowser> browser) OVERRIDE;

private:
    client::Window *mainWindow_;
    // List of existing browser windows. Only accessed on the CEF UI thread.
    typedef std::list<CefRefPtr<CefBrowser>> BrowserList;
    BrowserList browser_list_;
    CefRefPtr<CefJSDialogHandler> dialog_;
    CefRefPtr<client::ClientHandler> clientHandler_;
    bool is_closing_;

IMPLEMENT_REFCOUNTING(SimpleHandler);
    DISALLOW_COPY_AND_ASSIGN(SimpleHandler);
};

#endif  // CEF_TESTS_CEFSIMPLE_SIMPLE_HANDLER_H_
