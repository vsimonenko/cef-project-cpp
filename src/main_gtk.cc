#undef Success     // Definition conflicts with cef_message_router.h
#undef RootWindow  // Definition conflicts with root_window.h

#include <gtk/gtk.h>
#include <X11/Xlib.h>
#include <gtk/gtkgl.h>
#include <internal/cef_linux.h>
#include <cef_command_line.h>
#include <cef_app.h>
#include "main_app.h"

namespace client {
/*
 * The supported severity levels for macros LOG that allow you to specify one
 * are (in increasing order of severity) INFO, WARNING, ERROR, and FATAL.
 */

    client::WindowGtk CreateWindow(int argc, char **argv, CefRect *winRect) {
        gtk_init(&argc, &argv);
        gtk_gl_init(&argc, &argv);

        GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_window_set_title(GTK_WINDOW(window), "cef example");
        gtk_window_set_default_size(GTK_WINDOW(window), winRect->width, winRect->height);

        GdkColor color;
        color.red = 65535 >> 2;
        color.green = 65535;
        color.blue = 65535;
        gtk_widget_modify_bg(window, GTK_STATE_NORMAL, &color);

        gtk_widget_show_all(window);

        GdkWindow *gdk_window = gtk_widget_get_window(window);
        GdkDisplay *display = gdk_window_get_display(gdk_window);
        gdk_display_flush(display);

        client::WindowGtk clientWindow(winRect, window);

        return clientWindow;
    }

    gboolean delete_event(GtkWidget *widget, GdkEvent *event, MainApp *mainApp) {
        return mainApp->Close();
    }
}

// Program entry point function.
int main(int argc, char *argv[]) {
    CefRect rect(0, 0, 550, 500);

    //"js::TestJSFactory" is default handler of javascript
    CefRefPtr<MainApp> app(new MainApp(argc, argv, "js::TestJSFactory"));
    client::WindowGtk window = client::CreateWindow(argc, argv, &rect);

    SimpleHandler *simpleHandler = new SimpleHandler(&window,
                                                     new js::CefJSDialogHandlerDelegateGtk(&window),
                                                     new client::ClientHandler());
    if (!app->CreateBrowser(&window, "client://page1.html?JS_FACTORY_NAME=js::TestJSFactory", simpleHandler))
        return -1;
    rect.Set(rect.x + 10, rect.y + 10, rect.width - 20, rect.height - 20);
    window.SetBrowserRect(&rect);
    window.Show();
    g_signal_connect(G_OBJECT(window.GetTopWindow()), "delete_event", G_CALLBACK(&client::delete_event), app.get());
    g_signal_connect(G_OBJECT(window.GetTopWindow()), "size-allocate",
                     G_CALLBACK(&client::WindowGtk::CefBrowserSizeAllocated), &window);
    g_signal_connect(G_OBJECT(window.GetTopWindow()), "focus-in-event",
                     G_CALLBACK(&client::WindowGtk::WindowFocusIn), &window);
    g_signal_connect(G_OBJECT(window.GetTopWindow()), "configure-event",
                     G_CALLBACK(&client::WindowGtk::ChangeConfigure), &window);

    CefRunMessageLoop();

    DLOG(INFO) << "***** SHUTDOWN *****";
    CefShutdown();

    DLOG(INFO) << "***** EXIT *****";
    return 0;
}
