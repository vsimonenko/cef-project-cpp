#ifndef CEF_WINDOW_H
#define CEF_WINDOW_H

#include <internal/cef_linux.h>

namespace client {
    class Window {
    public:
        Window() {
            windowRect.Set(0, 0, 0, 0);
        }

        Window(CefRect *rect) {
            SetWindowRect(rect);
        }

        virtual ~Window() {};

        virtual CefWindowHandle GetXWindow() = 0;

        virtual void Show() = 0;

        virtual void PlatformTitleChange(CefRefPtr<CefBrowser> browser, const CefString &title) = 0;

        virtual void SetBrowserRect(CefRect *rect) {
            browserRect.Set(rect->x, rect->y, rect->width, rect->height);
        }

        CefRect GetBrowserRect() const {
            return browserRect;
        }

        CefRect GetWindowRect() const {
            return windowRect;
        }

        void SetWindowRect(CefRect *rect) {
            windowRect.Set(rect->x, rect->y, rect->width, rect->height);
        }

        const CefRefPtr<CefBrowser> GetBrowser() const {
            return browser;
        }

        void SetBrowser(CefRefPtr<CefBrowser> browser) {
            Window::browser = browser;
        }

    private:
        CefRect windowRect;
        CefRect browserRect;
        CefRefPtr<CefBrowser> browser;
    };
}
#endif //CEF_WINDOW_H
