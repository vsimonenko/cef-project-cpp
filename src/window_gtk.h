#ifndef CEF_WINDOW_GTK_H
#define CEF_WINDOW_GTK_H

#include <cef_browser.h>
#include "window.h"

namespace client {
    class WindowGtk : public Window {
    public:
        WindowGtk(CefRect *windowRect, GtkWidget *top_window);

        WindowGtk(GtkWidget *top_window);

        ~WindowGtk() = default;

        CefWindowHandle GetXWindow() override;

        GtkWidget *GetTopWindow();

        virtual void PlatformTitleChange(CefRefPtr<CefBrowser> browser, const CefString &title) override;

        virtual void SetBrowserRect(CefRect *rect) override;

        virtual void Show() override;

        void ChangeRect(GdkRectangle *newSize);

        static void CefBrowserSizeAllocated(GtkWidget *widget, GtkAllocation *allocation, WindowGtk *window);

        static void ChangeConfigure(GtkWidget *widget, GdkEvent *event, WindowGtk *window);

        static gboolean WindowFocusIn(GtkWidget *widget, GdkEventFocus *event, client::Window *window);

    private:
        WindowGtk() {};

        static void SetXWindowBounds(CefWindowHandle xwindow, int x, int y, size_t width, size_t height);

        static void SetXWindowBounds(CefWindowHandle xwindow, CefRect *rect);

        static void SetXWindowVisible(CefWindowHandle xwindow, bool visible);

        GtkWidget *top_window;
        int dx, dy, dw, dh;
    };
}

#endif //CEF_WINDOW_GTK_H
