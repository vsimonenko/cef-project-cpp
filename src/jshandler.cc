#include <cef_command_line.h>
#include <cef_parser.h>
#include <thread>
#include "jshandler.h"
#include "client_util.h"

namespace js {

#define registry(key, factory) ( factory_registration_map.insert(std::make_pair(key, factory)) )

    JSFactory::TypeJSFactoryMap JSFactory::factory_registration_map = JSFactory::init();

    //The method is called in different processes and threads.
    JSFactory::TypeJSFactoryMap JSFactory::init() {
        DLOG(INFO) << "process id : " << getpid() << ", thread id: " << std::this_thread::get_id();
        JSFactory::TypeJSFactoryMap factory_registration_map;
        //The registarion only have to work in TID RENDERER thread, so we comment followed code:
        //registry("js::TestJSFactory", new TestJSFactory());
        //navigating to page will call the "contextcreated" method.
        return factory_registration_map;
    }

    void JSFactory::RegisterCefV8HandlerFactory() {
        if (!factory_registration_map.empty())
            return;

        DLOG(INFO) << "***** JSFactory::RegisterCefV8HandlerFactory *****";

        registry("js::TestJSFactory", new TestJSFactory());
    }

    JSFactory *JSFactory::Find(std::string key) {
        TypeJSFactoryMap::iterator it;
        it = factory_registration_map.find(key);
        JSFactory *factory = (it != factory_registration_map.end()) ? ((*it).second) : nullptr;
        return factory;
    }

    void JSFactory::Release() {
        DLOG(INFO) << "***** JSFactory::Release is started*****";
        if (factory_registration_map.empty()) {
            DLOG(INFO) << "***** JSFactory::Release is finished*****";
            return;
        }

        for (auto it = factory_registration_map.begin(); it != factory_registration_map.end(); ++it) {
            delete (*it).second;
        }
        factory_registration_map.clear();
        DLOG(INFO) << "***** JSFactory::Release is finished*****";
    }

    bool TestCefV8Handler::Execute(const CefString &name, CefRefPtr<CefV8Value> object, const CefV8ValueList &arguments,
                                   CefRefPtr<CefV8Value> &retval, CefString &exception) {

        DLOG(INFO) << "TestCefV8Handler::Execute: name: " << name.ToString();

        if (name == "setMessageCallback") {
            if (arguments.size() == 2 && static_cast<CefV8Value *>(arguments[0])->IsString() &&
                static_cast<CefV8Value *>(arguments[1])->IsFunction()) {
                std::string message_name = static_cast<CefV8Value *>(arguments[0])->GetStringValue();
                CefRefPtr<CefV8Context> context = CefV8Context::GetCurrentContext();
                int browser_id = context->GetBrowser()->GetIdentifier();
                callbackMap->insert(
                        std::make_pair(std::make_pair(message_name, browser_id),
                                       std::make_pair(context, arguments[1])));
                return true;
            }
        }

        CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create(name);
        CefRefPtr<CefListValue> args = msg->GetArgumentList();
        for (size_t i = 0; i < arguments.size(); i++) {
            CefRefPtr<CefV8Value> v = arguments[i];
            if (v->IsString())
                args->SetString(i, v->GetStringValue());
            else if (v->IsInt())
                args->SetInt(i, v->GetIntValue());
            else if (v->IsDouble())
                args->SetDouble(i, v->GetDoubleValue());
        }
        SendMessage(CefV8Context::GetCurrentContext()->GetBrowser(), msg);

        return true;
    }

    bool SendMessage(CefRefPtr<CefBrowser> browser, CefRefPtr<CefProcessMessage> msg) {
        return browser->SendProcessMessage(PID_BROWSER, msg);
    }

    void Release(V8ContextCallbackMap &callbackMap, CefRefPtr<CefV8Context> context) {
        DLOG(INFO) << "TestCefV8Handler::Release";

        // Remove any JavaScript callbacks registered for the context that has been released.
        if (!callbackMap.empty()) {
            for (auto it = callbackMap.begin(); it != callbackMap.end();) {
                if (it->second.first->IsSame(context))
                    callbackMap.erase(it++);
                else
                    ++it;
            }
        }
    }

    bool ExecuteFunction(V8ContextCallbackMap &callbackMap, CefRefPtr<CefBrowser> browser, CefRefPtr<CefProcessMessage> message) {
        DLOG(INFO) << "TestCefV8Handler::ExecuteFunction";

        bool handled = false;
        if (!callbackMap.empty()) {
            const CefString &message_name = message->GetName();
            auto it = callbackMap.find(
                    std::make_pair(message_name.ToString(),
                                   browser->GetIdentifier()));
            if (it != callbackMap.end()) {
                // Keep a local reference to the objects. The callback may remove itself
                // from the callback map.
                CefRefPtr<CefV8Context> context = it->second.first;
                CefRefPtr<CefV8Value> callback = it->second.second;

                // Enter the context.
                context->Enter();

                CefV8ValueList arguments;

                // First argument is the message name.
                arguments.push_back(CefV8Value::CreateString(message_name));

                // Second argument is the list of message arguments.
                CefRefPtr<CefListValue> list = message->GetArgumentList();
                CefRefPtr<CefV8Value> args = CefV8Value::CreateArray(list->GetSize());
                shared::SetList(list, args);  // Helper function to convert CefListValue to CefV8Value.
                arguments.push_back(args);

                // Execute the callback.
                CefRefPtr<CefV8Value> retval = callback->ExecuteFunction(nullptr, arguments);
                if (retval.get()) {
                    if (retval->IsBool())
                        handled = retval->GetBoolValue();
                }

                // Exit the context.
                context->Exit();
            }
        }
        return handled;
    }

    TestCefV8Handler::TestCefV8Handler(V8ContextCallbackMap &callbackMap) : callbackMap(&callbackMap) {}

    void TestJSFactory::CreateContext(V8ContextCallbackMap &callbackMap, CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
                                                             CefRefPtr<CefV8Context> context) {
        DLOG(INFO) << "***** TestJSFactory::CreateContext *****";

        CefRefPtr<CefV8Value> object = context->GetGlobal();
        // Create a new V8 string value. See the "Basic JS Types" section below.
        CefRefPtr<CefV8Value> str = CefV8Value::CreateString("Hello!");
        // Add the string to the window object as "window.myval". See the "JS Objects" section below.
        object->SetValue("cefvalue", str, V8_PROPERTY_ATTRIBUTE_NONE);


        CefRefPtr<TestCefV8Handler> testCefV8Handler = new TestCefV8Handler(callbackMap);

        // Create the "calculate" function.
        CefRefPtr<CefV8Value> func = CefV8Value::CreateFunction("calculate", testCefV8Handler);
        // Add the "calculate" function to the "window" object.
        object->SetValue("calculate", func, V8_PROPERTY_ATTRIBUTE_NONE);

        func = CefV8Value::CreateFunction("to_page", testCefV8Handler);
        object->SetValue("to_page", func, V8_PROPERTY_ATTRIBUTE_NONE);

        func = CefV8Value::CreateFunction("reload", testCefV8Handler);
        // Add the "reload" function to the "window" object.
        object->SetValue("reload", func, V8_PROPERTY_ATTRIBUTE_NONE);

        func = CefV8Value::CreateFunction("setMessageCallback", testCefV8Handler);
        // Add the "reload" function to the "window" object.
        object->SetValue("setMessageCallback", func, V8_PROPERTY_ATTRIBUTE_NONE);

        CefURLParts parts;
        shared::ParseUrl(browser->GetMainFrame()->GetURL(), parts);
        CefRefPtr<CefV8Accessor> testV8Accessor(new TestV8Accessor(parts));
        CefRefPtr<CefV8Value> accessor = CefV8Value::CreateObject(testV8Accessor, nullptr);
        accessor->SetValue("request", V8_ACCESS_CONTROL_DEFAULT, V8_PROPERTY_ATTRIBUTE_NONE);
        object->SetValue("accessor", accessor, V8_PROPERTY_ATTRIBUTE_NONE);
    }

    bool TestV8Accessor::Get(const CefString &name, const CefRefPtr<CefV8Value> object, CefRefPtr<CefV8Value> &retval,
                             CefString &exception) {
        if (name == "request") {
            std::string path = shared::GetPathFromURL(cefURLParts);
            if (!path.empty()) {
                std::string su = "{\"path\" : \"" + path + "\",\"query\" : \"" + CefString(&cefURLParts.query).ToString() + "\"}";
                CefRefPtr<CefValue> value = CefParseJSON(su, JSON_PARSER_RFC);
                retval = shared::CefValueToCefV8Value(value);
                return true;
                //test_request.FromString("");
                //retval = CefV8Value::CreateString(test_request);
                //return true;
            }
        }
        return false;
    }

    bool TestV8Accessor::Set(const CefString &name,
                             const CefRefPtr<CefV8Value> object,
                             const CefRefPtr<CefV8Value> value,
                             CefString &exception) {
        if (name == "request") {
            if (value->IsString()) {
                // Store the value.
                //DLOG(INFO) << "TestV8Accessor::Set: " << value->GetStringValue().ToString();
                //test_request = value->GetStringValue();
            } else {
                // Throw an exception.
                exception = "Invalid value type";
            }
            return true;
        }
        return false;
    }

    TestV8Accessor::TestV8Accessor(CefURLParts cefURLParts) : cefURLParts(cefURLParts) {
        test_request.FromString(" ");
    };
}