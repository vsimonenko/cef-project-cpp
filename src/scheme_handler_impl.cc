// Copyright (c) 2017 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include <cef_parser.h>
#include "scheme_handler_impl.h"

#include "include/cef_browser.h"
#include "include/cef_resource_handler.h"
#include "include/cef_scheme.h"
#include "include/wrapper/cef_helpers.h"

#include "resource_util.h"
#include "client_util.h"

namespace scheme_handler {

    static const std::string scheme = "client";
    static const unsigned int scheme_size = scheme.size() + 3;

// Implementation of the scheme handler for client:// requests.
    class ClientSchemeHandler : public CefResourceHandler {
    public:
        ClientSchemeHandler() : offset_(0) {}

        bool ProcessRequest(CefRefPtr<CefRequest> request,
                            CefRefPtr<CefCallback> callback) OVERRIDE {
            CEF_REQUIRE_IO_THREAD();

            bool handled = false;

            std::string path = shared::GetPathFromURL(request->GetURL());
            if (path.rfind(".html") != std::string::npos) {
                // Load the response html.
                if (shared::GetResourceString(path, data_)) {
                    // Insert the request contents.
                    const std::string &find = "$REQUEST$";
                    const std::string &replace = shared::DumpRequestContents(request);

                    handled = true;
                    mime_type_ = "text/html";
                }
            } else if (path.rfind(".png") != std::string::npos) {
                // Load the response image.
                if (shared::GetResourceString(path, data_)) {
                    handled = true;
                    mime_type_ = "image/png";
                }
            }

            if (handled) {
                // Indicate that the headers are available.
                callback->Continue();
                return true;
            }

            return false;
        }

        void GetResponseHeaders(CefRefPtr<CefResponse> response,
                                int64 &response_length,
                                CefString &redirectUrl) OVERRIDE {
            CEF_REQUIRE_IO_THREAD();

            DCHECK(!data_.empty());

            response->SetMimeType(mime_type_);
            response->SetStatus(200);

            // Set the resulting response length.
            response_length = data_.length();
        }

        void Cancel() OVERRIDE { CEF_REQUIRE_IO_THREAD(); }

        bool ReadResponse(void *data_out,
                          int bytes_to_read,
                          int &bytes_read,
                          CefRefPtr<CefCallback> callback) OVERRIDE {
            CEF_REQUIRE_IO_THREAD();

            bool has_data = false;
            bytes_read = 0;

            if (offset_ < data_.length()) {
                // Copy the next block of data into the buffer.
                int transfer_size =
                        std::min(bytes_to_read, static_cast<int>(data_.length() - offset_));
                memcpy(data_out, data_.c_str() + offset_, transfer_size);
                offset_ += transfer_size;

                bytes_read = transfer_size;
                has_data = true;
            }

            return has_data;
        }

    private:
        std::string data_;
        std::string mime_type_;
        size_t offset_;

    IMPLEMENT_REFCOUNTING(ClientSchemeHandler);
        DISALLOW_COPY_AND_ASSIGN(ClientSchemeHandler);
    };

// Implementation of the factory for creating scheme handlers.
    class ClientSchemeHandlerFactory : public CefSchemeHandlerFactory {
    public:
        ClientSchemeHandlerFactory() {}

        // Return a new scheme handler instance to handle the request.
        CefRefPtr<CefResourceHandler> Create(CefRefPtr<CefBrowser> browser,
                                             CefRefPtr<CefFrame> frame,
                                             const CefString &scheme_name,
                                             CefRefPtr<CefRequest> request) OVERRIDE {
            CEF_REQUIRE_IO_THREAD();
            return new ClientSchemeHandler();
        }

    private:
    IMPLEMENT_REFCOUNTING(ClientSchemeHandlerFactory);
        DISALLOW_COPY_AND_ASSIGN(ClientSchemeHandlerFactory);
    };

    void RegisterSchemeHandlerFactory() {
        CefRegisterSchemeHandlerFactory(scheme, "test",
                                        new ClientSchemeHandlerFactory());
    }

}  // namespace scheme_handler
