#ifndef CEF_EXAMPLES_SHARED_CLIENT_BASE_H_
#define CEF_EXAMPLES_SHARED_CLIENT_BASE_H_

#include <gtk/gtkstyle.h>
#include <gtk/gtkwidget.h>
#include "include/cef_client.h"
#include "main_app.h"

namespace shared {

// Returns the contents of |request| as a string.
    std::string DumpRequestContents(CefRefPtr<CefRequest> request);
    CefRefPtr<CefV8Value> CefValueToCefV8Value(CefRefPtr<CefValue> value);
    bool ParseUrl(CefString cefUrl, CefURLParts &parts);
    bool ParseUrl(std::string url, CefURLParts &parts);
    std::string GetPathFromURL(CefURLParts &parts);
    std::string GetPathFromURL(CefString url);
    std::map<std::string, std::string> *GetParamsFromURL(CefURLParts &parts);
    void SetList(CefRefPtr<CefListValue> list, CefRefPtr<CefV8Value> args);
}  // namespace shared

#endif  // CEF_EXAMPLES_SHARED_CLIENT_BASE_H_
