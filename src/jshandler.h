#ifndef CEF_JSHANDLER_H
#define CEF_JSHANDLER_H

#include <cef_browser.h>
#include <internal/cef_string.h>
#include <cef_v8.h>
#include <cef_render_process_handler.h>
#include <base/cef_lock.h>

namespace js {

    typedef std::map<std::pair<std::string, int>,
            std::pair<CefRefPtr<CefV8Context>, CefRefPtr<CefV8Value> > >
            V8ContextCallbackMap;

    bool ExecuteFunction(V8ContextCallbackMap &callbackMap, CefRefPtr<CefBrowser> browser, CefRefPtr<CefProcessMessage> message);
    bool SendMessage(CefRefPtr<CefBrowser> browser, CefRefPtr<CefProcessMessage> msg);
    void Release(V8ContextCallbackMap &callbackMap, CefRefPtr<CefV8Context> context);

    class JSFactory {
    public:
        virtual void CreateContext(
                V8ContextCallbackMap &callbackMap,
                CefRefPtr<CefBrowser> browser,
                CefRefPtr<CefFrame> frame,
                CefRefPtr<CefV8Context> context) = 0;

        virtual ~JSFactory() = default;

        static void RegisterCefV8HandlerFactory();

        static JSFactory *Find(std::string key);

        static void Release();

    private:
        //static void Registry(std::string key, JSFactory *factory);
        typedef std::map<std::string, JSFactory *> TypeJSFactoryMap;
        static TypeJSFactoryMap factory_registration_map;

        static TypeJSFactoryMap init();
    };

    class TestCefV8Handler : public CefV8Handler {
    public:

        explicit TestCefV8Handler(V8ContextCallbackMap &callbackMap);

        bool Execute(const CefString &name, CefRefPtr<CefV8Value> object, const CefV8ValueList &arguments,
                     CefRefPtr<CefV8Value> &retval, CefString &exception) override;
    private:
        V8ContextCallbackMap *callbackMap;
    IMPLEMENT_REFCOUNTING(TestCefV8Handler);
    };

    class TestV8Accessor : public CefV8Accessor {
    public:
        explicit TestV8Accessor(CefURLParts cefURLParts);

        bool Get(const CefString &name,
                 const CefRefPtr<CefV8Value> object,
                 CefRefPtr<CefV8Value> &retval,
                 CefString &exception) OVERRIDE;

        bool Set(const CefString &name,
                 const CefRefPtr<CefV8Value> object,
                 const CefRefPtr<CefV8Value> value,
                 CefString &exception) OVERRIDE;

        CefString test_request;
        CefURLParts cefURLParts;

    IMPLEMENT_REFCOUNTING(TestV8Accessor);
    };

    class TestJSFactory : public JSFactory {
        void CreateContext(
                V8ContextCallbackMap &callbackMap,
                CefRefPtr<CefBrowser> browser,
                CefRefPtr<CefFrame> frame,
                CefRefPtr<CefV8Context> context) override;
    };
}


#endif //CEF_JSHANDLER_H
