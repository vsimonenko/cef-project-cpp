#include "simple_handler.h"
#include "main_app.h"

#include "include/views/cef_browser_view.h"
#include "include/wrapper/cef_closure_task.h"


namespace {


}  // namespace

SimpleHandler::SimpleHandler(client::Window *window,
                             CefRefPtr<CefJSDialogHandler> dialogHandler, client::ClientHandler *clientHandler)
        : mainWindow_(window), dialog_(dialogHandler),
          clientHandler_(clientHandler), is_closing_(false) {
}

SimpleHandler::~SimpleHandler() {}

void SimpleHandler::OnTitleChange(CefRefPtr<CefBrowser> browser,
                                  const CefString &title) {
    CEF_REQUIRE_UI_THREAD();
    mainWindow_->PlatformTitleChange(browser, title);
}

void SimpleHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser) {
    CEF_REQUIRE_UI_THREAD();

    // Add to the list of existing browsers.
    browser_list_.push_back(browser);
    if (!mainWindow_->GetBrowser())
        mainWindow_->SetBrowser(browser);
}

bool SimpleHandler::DoClose(CefRefPtr<CefBrowser> browser) {
    CEF_REQUIRE_UI_THREAD();

    // Closing the main window requires special handling. See the DoClose()
    // documentation in the CEF header for a detailed destription of this
    // process.
    if (browser_list_.size() == 1) {
        // Set a flag to indicate that the window close should be allowed.
        is_closing_ = true;
    }

    // Allow the close. For windowed browsers this will result in the OS close
    // event being sent.
    return false;
}

void SimpleHandler::OnBeforeClose(CefRefPtr<CefBrowser> browser) {
    CEF_REQUIRE_UI_THREAD();

    DLOG(INFO) << "SimpleHandler::OnBeforeClose, browser id: " << browser->GetIdentifier();

    if (mainWindow_->GetBrowser() && mainWindow_->GetBrowser()->IsSame(browser))
        mainWindow_->SetBrowser(nullptr);
    else if (mainWindow_->GetBrowser())
        mainWindow_->GetBrowser()->GetHost()->SetFocus(true);

    // Remove from the list of existing browsers.
    for (auto bit = browser_list_.begin(); bit != browser_list_.end(); ++bit) {
        if ((*bit)->IsSame(browser)) {
            browser_list_.erase(bit);
            break;
        }
    }

    if (browser_list_.empty()) {
        // All browser windows have closed. Quit the application message loop.
        CefQuitMessageLoop();
    }
}

void SimpleHandler::OnLoadError(CefRefPtr<CefBrowser> browser,
                                CefRefPtr<CefFrame> frame,
                                ErrorCode errorCode,
                                const CefString &errorText,
                                const CefString &failedUrl) {
    CEF_REQUIRE_UI_THREAD();

    // Don't display an error for downloaded files.
    if (errorCode == ERR_ABORTED)
        return;

    // Display a load error message.
    std::stringstream ss;
    ss << "<html><body bgcolor=\"white\">"
          "<h2>Failed to load URL "
       << std::string(failedUrl) << " with error " << std::string(errorText)
       << " (" << errorCode << ").</h2></body></html>";
    frame->LoadString(ss.str(), failedUrl);
}

void SimpleHandler::CloseAllBrowsers(bool force_close) {
    if (!CefCurrentlyOn(TID_UI)) {
        // Execute on the UI thread.
        CefPostTask(TID_UI, base::Bind(&SimpleHandler::CloseAllBrowsers, this,
                                       force_close));
        return;
    }

    if (browser_list_.empty())
        return;

    DLOG(INFO) << "SimpleHandler::CloseAllBrowsers";
    BrowserList::const_iterator it = browser_list_.begin();
    for (; it != browser_list_.end(); ++it) {
        (*it)->GetHost()->CloseBrowser(force_close);
    }
}

void SimpleHandler::OnResetDialogState(CefRefPtr<CefBrowser> browser) {
    CEF_REQUIRE_UI_THREAD();
    dialog_->OnResetDialogState(browser);
}

bool SimpleHandler::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
                                             CefRefPtr<CefProcessMessage> message) {

    DLOG(INFO) << "SimpleHandler::OnProcessMessageReceived";
    DLOG(INFO) << "browser id: " << browser->GetIdentifier();

    std::string name = message->GetName().ToString();
    if (CefCurrentlyOn(TID_UI)) {
        if (name == "set_focus") {
            browser.get()->GetHost()->SetFocus(true);
            browser.get()->GetHost()->SendFocusEvent(true);
            return true;
        }
        if (name == "reload") {
            browser->Reload();
            browser->GetHost()->SetFocus(true);
            browser->GetHost()->SendFocusEvent(true);
            return true;
        }
        return clientHandler_->OnProcessMessageReceived(browser, source_process, message);
    }
    if (CefCurrentlyOn(TID_RENDERER)) {
        return false;
    }
    return false;
}
