// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "main_app.h"
#include "include/views/cef_browser_view.h"
#include "scheme_handler_impl.h"
#include "client_util.h"

MainApp::MainApp(int argc, char **argv, std::string js_factory_name) {
    // Structure for passing command-line arguments.
    // The definition of this structure is platform-specific.
    CefMainArgs main_args(argc, argv);

    this->js_factory_name = js_factory_name;

    // Parse command-line arguments.
    CefRefPtr<CefCommandLine> command_line = CefCommandLine::CreateCommandLine();
    command_line->InitFromArgv(argc, reinterpret_cast<const char *const *>(argv));

    // Populate this structure to customize CEF behavior.
    CefSettings settings;
    settings.single_process = false;
    //settings.log_severity = LOGSEVERITY_VERBOSE;
    settings.log_severity = LOGSEVERITY_INFO;
    if (!command_line->HasSwitch("log")) {
        CefString(&settings.log_file).FromString(command_line->GetProgram().ToString() + ".log");
    }
    if (!command_line->HasSwitch("lang")) {
        CefString(&settings.locale).FromASCII("en-US");
    }
    if (!CefInitialize(main_args, settings, this, nullptr))
        exit(-1);
}


void MainApp::OnContextInitialized() {
    CEF_REQUIRE_UI_THREAD();
    scheme_handler::RegisterSchemeHandlerFactory();
}

bool MainApp::CreateBrowser(client::Window *window,
                            const std::string &def_url,
                            SimpleHandler *simpleHandler) {
    CEF_REQUIRE_UI_THREAD();

    CefRefPtr<CefCommandLine> command_line =
            CefCommandLine::GetGlobalCommandLine();

    MainApp::simpleHandler = simpleHandler;

    // Specify CEF browser settings here.
    CefBrowserSettings browser_settings;

    // Check if a "--url=" value was provided via the command-line. If so, use
    // that instead of the default URL.
    std::string url = command_line->GetSwitchValue("url");
    if (url.empty())
        url = def_url;

    CefWindowInfo window_info;
    window_info.SetAsChild(window->GetXWindow(), window->GetWindowRect());

    CefRefPtr<CefBrowser> browser = CefBrowserHost::CreateBrowserSync(window_info, simpleHandler, url, browser_settings,
                                                                      nullptr);
    return browser && browser.get();
}

void MainApp::OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
                              CefRefPtr<CefDOMNode> node) {
    CefRefPtr<CefProcessMessage> msg = CefProcessMessage::Create("change_focus");
    browser.get()->SendProcessMessage(PID_BROWSER, msg);
}

bool MainApp::Close() {
    if (!simpleHandler->IsClosing()) {
        simpleHandler->CloseAllBrowsers(false);
        return TRUE;
    }
    return FALSE;
}

void MainApp::OnContextCreated(CefRefPtr<CefBrowser> browser,
                               CefRefPtr<CefFrame> frame,
                               CefRefPtr<CefV8Context> context) {
    CEF_REQUIRE_RENDERER_THREAD();

    DLOG(INFO) << "MainApp::OnContextCreated, id: " << browser->GetIdentifier();

    js::JSFactory::RegisterCefV8HandlerFactory();
    js::JSFactory *jsFactory = nullptr;
    CefURLParts parts;
    if (shared::ParseUrl(browser->GetMainFrame()->GetURL(), parts)) {
        std::map<std::string, std::string> *params = shared::GetParamsFromURL(parts);
        auto it = params->find("JS_FACTORY_NAME");
        if (it != params->end()) {
            jsFactory = js::JSFactory::Find((*it).second);
        }
        delete params;
    }
    if (!jsFactory)
        jsFactory = js::JSFactory::Find(js_factory_name);
    DCHECK(jsFactory);
    jsFactory->CreateContext(callbackMap, browser, frame, context);
}

void MainApp::OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) {
    js::Release(callbackMap, context);
}

bool MainApp::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
                                       CefRefPtr<CefProcessMessage> message) {
    DLOG(INFO) << "MainApp::OnProcessMessageReceived, id: " << browser->GetIdentifier();
    return js::ExecuteFunction(callbackMap, browser, message);
}
