include_directories(${CEF_ROOT}/include)

# cefsimple sources.
set(CEFSIMPLE_SRCS_LINUX
        main_gtk.cc
        main_app.cc
        simple_handler.cc
        scheme_handler_impl.cc
        client_util.cc
        window_gtk.cc
        dialog_gtk.cc
        jshandler.cc
        client.cc
        resource_util_linux.cc
        resource_util_posix.cc
        resource_util.cc
        )
APPEND_PLATFORM_SOURCES(CEFSIMPLE_SRCS)
source_group(cefsimple FILES ${CEFSIMPLE_SRCS})

set(CEFSIMPLE_SRCS
        ${CEFSIMPLE_SRCS_LINUX}
        )

# Target executable names.
set(CEF_TARGET "cefsimple")

# Logical target used to link the libcef library.
ADD_LOGICAL_TARGET("libcef_lib" "${CEF_LIB_DEBUG}" "${CEF_LIB_RELEASE}")

# Determine the target output directory.
SET_CEF_TARGET_OUT_DIR()

#
# Linux configuration.
#

set(EXE_LINKER_FLAGS "${EXE_LINKER_FLAGS}  -lcef_dll_wrapper -L${CEF_TARGET_OUT_DIR} -L${CMAKE_CURRENT_BINARY_DIR}/../libcef_dll_wrapper")

message("EXE_LINKER_FLAGS: " ${EXE_LINKER_FLAGS})

if (OS_LINUX)
    FIND_LINUX_LIBRARIES("glib-2.0")

    message("***** OS_LINUX *****" "")

    # Executable target.
    add_executable(${CEF_TARGET} ${CEFSIMPLE_SRCS})
    # Copy binary and resource files to the target output directory.

    SET_EXECUTABLE_TARGET_PROPERTIES(${CEF_TARGET})
    add_dependencies(${CEF_TARGET} libcef_dll_wrapper)
    target_link_libraries(${CEF_TARGET} libcef_lib libcef_dll_wrapper ${CEF_STANDARD_LIBS})

    # Set rpath so that libraries can be placed next to the executable.
    set_target_properties(${CEF_TARGET} PROPERTIES INSTALL_RPATH "$ORIGIN")
    set_target_properties(${CEF_TARGET} PROPERTIES LINK_LIBRARIES ${EXE_LINKER_FLAGS})
    set_target_properties(${CEF_TARGET} PROPERTIES BUILD_WITH_INSTALL_RPATH TRUE)
    set_target_properties(${CEF_TARGET} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CEF_TARGET_OUT_DIR})

    set(CMD_COPY1 COMMAND cp -r "${CEF_BINARY_DIR}/*" "${CEF_TARGET_OUT_DIR}/")
    set(CMD_COPY2 COMMAND cp -r "${CEF_ROOT}/Resources/*" "${CEF_TARGET_OUT_DIR}/")
    set(CMD_COPY3 COMMAND cp -r "${CMAKE_SOURCE_DIR}/resources/*" "${CEF_TARGET_OUT_DIR}/resources")
    set(CMD_RES1 COMMAND mkdir -p "${CEF_TARGET_OUT_DIR}/resources")
    add_custom_command(
            TARGET ${CEF_TARGET}
            PRE_BUILD
            ${CMD_RES1}
            ${CMD_COPY1}
            ${CMD_COPY2}
            ${CMD_COPY3}
    )
    # Copy binary and resource files to the target output directory.
    COPY_FILES("${CEF_TARGET}" "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CEF_TARGET_OUT_DIR}")    

    # Set SUID permissions on the chrome-sandbox target.
    SET_LINUX_SUID_PERMISSIONS("${CEF_TARGET}" "${CEF_TARGET_OUT_DIR}/chrome-sandbox")

    install(TARGETS ${CEF_TARGET} DESTINATION ${INSTALL_DIRECTORY})
endif ()
