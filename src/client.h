#ifndef CEF_CLIENT_H
#define CEF_CLIENT_H

#include <cef_process_message.h>
#include <cef_browser.h>

namespace client {

    class ClientHandler : public virtual CefBaseRefCounted {
    public:
        bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
                                      CefRefPtr<CefProcessMessage> message);

    private:
    IMPLEMENT_REFCOUNTING(ClientHandler);
    };

}


#endif //CEF_CLIENT_H
